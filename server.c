
#define _GNU_SOURCE //Want accept4()

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define BUFF_LEN 1024

const uint32_t BACKLOG = 10;

enum prog_arg {
	PROG_DIR,
	PORT,
	INTERVAL,
	__ARG_COUNT
};

struct prog_state {
	uint32_t port;
	uint32_t interval;
	struct worker_node *workers;
};

struct worker {
	uint32_t num;
	int socket;
};

struct worker_node {
	struct worker worker;
	struct worker_node *next;
};

void kill_signal_hndl (int32_t signal);
int32_t main_get_state (struct prog_state *state, uint32_t argc, char *argv[]);

int32_t add_worker (struct worker_node **root, struct worker worker);
void free_workers (struct worker_node *root);

int main (uint32_t argc, char *argv[])
{
	signal(SIGINT, kill_signal_hndl);

	struct prog_state state;
	if (main_get_state(&state, argc, argv) < 0) {
		fprintf(
			stderr,
			"Usage: %s [PORT] [INTERVAL_MS]\n",
			argv[PROG_DIR]
		);
		exit(EXIT_FAILURE);
	} else {
		printf(
			"Starting server at :%u with a %ums interval...\n",
			state.port,
			state.interval
		);
	}
	state.workers = 0;

#ifdef VERBOSE
	printf(">>>\tServer: calling socket(protocol_domain, protocol_type, exact_protocol)\n");
#endif

	int srv_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (srv_sock == 0) {
		fprintf(stderr, "Server failed to create a socket\n");
		exit(EXIT_FAILURE);
	}

#ifdef VERBOSE
	printf(">>>\tServer: int srv_sock = socket(AF_INET, SOCK_STREAM, 0)\n");
	printf(">>>\tServer: Setting the socket to be non-blocking\n");
#endif

	int32_t flags = fcntl(srv_sock, F_GETFL, 0);
	if (flags < 0) {
		fprintf(stderr, "Could not retrieve flags for socket file descriptor\n");
		exit(EXIT_FAILURE);
	}
	flags |= O_NONBLOCK;
	if (fcntl(srv_sock, F_SETFL, flags) < 0) {
		fprintf(stderr, "Could not set non-blocking status for socket\n");
		exit(EXIT_FAILURE);
	}

#ifdef VERBOSE
	printf(">>>\tServer: Specify to force use the local address and specific port\n");
#endif

	int32_t opt = 1;
	if (setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) != 0) {
		fprintf(stderr, "Could not set socket options\n");
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(state.port);

#ifdef VERBOSE
	printf(">>>\tServer: Configuring to forward data from the address to this socket\n");
	printf(">>>\tServer: calling bind(socket, address, address_length)\n");
#endif

	if (bind(srv_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		fprintf(stderr, "Failed to bind to port\n");
		exit(EXIT_FAILURE);
	}

#ifdef VERBOSE
	printf(">>>\tServer: bind(srv_sock, &addr, sizeof(addr))\n");
	printf(">>>\tServer: Setting the socket to be PASSIVE which shows it will accept incoming data\n");
	printf(">>>\tServer: calling listen(socket, backlog_count)\n");
#endif

	if (listen(srv_sock, BACKLOG) < 0) {
		fprintf(stderr, "Failed to start listen\n");
		exit(EXIT_FAILURE);
	}

#ifdef VERBOSE
	printf(">>>\tServer: listen(srv_sock, BACKLOG)\n");
#endif

	printf("Accepting new clients and listening...\n");

#ifdef VERBOSE
	printf(">>>\tServer: Entering infinite loop to accept and poll clients\n");
#endif

	struct timespec stag_delay = {
		.tv_sec = state.interval / 1000,
		.tv_nsec = (state.interval % 1000) * 1000000
	};

	struct worker worker;
	uint32_t worker_count = 0;
	char buff[BUFF_LEN] = { 0 };
	int32_t in_len = 0;
	uint32_t addr_len = sizeof(addr);
	while (1) {
#ifdef VERBOSE
		printf(">>>\t\tServer: Polling for next client (non-blocking)\n");
		printf(">>>\t\tServer: calling accept(srv_sock, &addr, &addr_len)\n");
#endif
		worker.socket = accept4(srv_sock, (struct sockaddr *)&addr, (socklen_t *)&addr_len, SOCK_NONBLOCK);
		worker.num = worker_count;

		if (worker.socket < 0 && errno != EAGAIN && errno != EWOULDBLOCK) {
			fprintf(stderr, ">\tFailed to accept new client\n"); 
		} else if (worker.socket >= 0) {
#ifdef VERBOSE
			printf(">>>\t\tServer: New client available! Getting their new, personal socket and adding to client list\n");
#endif
			if (add_worker(&(state.workers), worker) < 0) {
				fprintf(stderr, ">\tFailed to add worker to list\n");
			} else {
				printf(">\tAccepted new client %u!\n", worker_count);
				worker_count++;
			}
		} else {
#ifdef VERBOSE
			printf(">>>\t\tServer: accept() returned no new clients\n");
#endif
		}

		time_t now;
		time(&now);
		struct tm *local = localtime(&now);
		
		printf(">\tPolling clients at %02d:%02d:%02d...\n", local->tm_hour, local->tm_min, local->tm_sec);

		struct worker_node **wn = &(state.workers);
		struct worker *w;
		while (*wn != 0) {
			w = &((*wn)->worker);
#ifdef VERBOSE
			printf(">>>\t\t\tServer: polling client %u (non-blocking)\n", w->num);
			printf(">>>\t\t\tServer: calling read(alternate_socket, buffer, length)\n");
#endif
			if (in_len > 0)
				memset(buff, 0, in_len);

			in_len = read(w->socket, buff, BUFF_LEN - 1);
			if (in_len < 0 && errno != EAGAIN && errno != EWOULDBLOCK) {
				in_len = 0;
				fprintf(stderr, ">\tFailed to read from client %u\n", w->num);
			} else if (in_len > 0) {
				printf(">\tClient %u said: '%s'\n", w->num, buff); 
			} else if (in_len == 0) {
				printf(">\tClient %u disconnected\n", w->num);
				close(w->socket);
				// Remove one with pointer magic
				struct worker_node *temp_node = *wn;
				*wn = (*wn)->next;
				free(temp_node);
				continue;
			} else {
#ifdef VERBOSE
				printf(">>>\t\t\tServer: Client %u did not send anything\n", w->num);
#endif
			}

			wn = &((*wn)->next);
		}
#ifdef VERBOSE
		printf(">>>\t\tServer: Sleeping\n");
#endif
		nanosleep(&stag_delay, &stag_delay);
	}
	
	fprintf(stderr, "Server broke free from infinite loop!\n");
	
	free_workers(state.workers);
	exit(EXIT_FAILURE);
	return 0;
}

int32_t add_worker (struct worker_node **root, struct worker worker)
{
	struct worker_node *new_node;
	if ((new_node = malloc(sizeof(*new_node))) == 0)
		return -1;

	new_node->worker = worker;
	new_node->next = 0;

	struct worker_node **n = root;
	while (*n != 0)
		n = &((*n)->next);
	*n = new_node;
	return 0;
}

void free_workers (struct worker_node *root)
{
	struct worker_node *n;
	while (root->next != 0) {
		n = root->next;
		free(root);
		root = n;
	}
}

int32_t main_get_state (struct prog_state *state, uint32_t argc, char *argv[])
{
	if (argc != __ARG_COUNT)
		return -1;

	state->port = (uint32_t)atoi(argv[PORT]);
	state->interval = (uint32_t)atoi(argv[INTERVAL]);

	return 0;	
}

void kill_signal_hndl (int32_t signal)
{
	printf("Aborting\n");
	kill(-1 * getpid(), SIGKILL);
	exit(EXIT_SUCCESS);
}


