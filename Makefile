#!/usr/bin/make

VERBOSE ?= false
DEBUG ?= false
GCC_ADD_FLAGS =

ifeq ($(VERBOSE),true)
	GCC_ADD_FLAGS += -DVERBOSE
endif

ifeq ($(DEBUG),true)
	GCC_ADD_FLAGS += -g -DDEBUG
endif

all: server.c client.c
	gcc $(GCC_ADD_FLAGS) -o server server.c
	gcc $(GCC_ADD_FLAGS) -o client client.c

.PHONY: clean

clean:
	rm server client
