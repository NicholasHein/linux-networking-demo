
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>

const char *SERVER_ADDR = "127.0.0.1";
const uint32_t MSG_LEN = 100;

struct prog_state {
	uint32_t num_client;
	uint32_t interval;
	uint32_t srv_port;
	uint32_t cli_num;
	int out_pipe;
	int in_pipe;
};

enum prog_arg {
	PROG_DIR,
	NUM_CLIENTS,
	INTERVAL,
	SRV_PORT,
	__ARG_COUNT
};

struct child_client {
	pid_t pid;
	int in_pipe;
};

int32_t main_get_state (struct prog_state *state, uint32_t argc, char *argv[]);
int32_t main_fork_clients (struct child_client *clients, struct prog_state *state);

int32_t parent_wait (struct child_client *clients, struct prog_state state);

void run_client (struct prog_state state);

void kill_signal_hndl (int32_t signal); 
void child_signal_hndl (int32_t signal);

uint32_t child_count;

int main (uint32_t argc, char *argv[])
{
	// Catch kill signal
	signal(SIGINT, kill_signal_hndl);
	signal(SIGCHLD, child_signal_hndl);
	
	// Parse parameters
	struct prog_state state;
	if (main_get_state(&state, argc, argv) < 0) {
		// Failed to parse parameters
		fprintf(
			stderr, 
			"Usage: %s [NUM_CLIENTS] [INTERVAL_MS] [SERVER_PORT]\n",
			argv[PROG_DIR]
		);
		exit(EXIT_FAILURE);
	} else {
		// Succeeded
		printf(
			"Starting %u clients sending to :%u at a %ums interval...\n",
			state.num_client,
			state.srv_port,
			state.interval
		);
	}
	
	// Allocate client buffer
	struct child_client *clients = malloc(sizeof(struct child_client) * state.num_client);
	if (clients == 0) {
		fprintf(stderr, "Could not allocate client buffer!\n");
		exit(EXIT_FAILURE);
	}

	// Fork all clients
	int32_t ex_res = EXIT_SUCCESS;
	int32_t fork_res = main_fork_clients(clients, &state);
	if (fork_res < 0) {
		// Forking failed
		fprintf(stderr, "Failed to fork clients\n");
		ex_res = EXIT_FAILURE;
	} else if (fork_res != 0) {
		// Fork, is parent
		// Service and wait for children
		if (parent_wait(clients, state) < 0) {
			// Failed to service
			fprintf(stderr, "Servicing failed!\n");
			ex_res = EXIT_FAILURE;
		} else {
			printf("Servicing completed\n");
		}
	} else {
		// Fork, is child
		run_client(state);
		printf("Client %u completed\n", state.cli_num);
	}

	kill(-1 * getpid(), SIGKILL);
	free(clients);
	exit(ex_res);
	return 0;
}

void kill_signal_hndl (int32_t signal) 
{
	printf("Aborting.\n");
	kill(-1 * getpid(), SIGKILL);
	exit(EXIT_SUCCESS);
}

void child_signal_hndl (int32_t signal)
{
	printf("Child exited\n");
	child_count--;
}

int32_t main_get_state (struct prog_state *state, uint32_t argc, char *argv[])
{
	if (argc != __ARG_COUNT)
		return -1;

	state->num_client = (uint32_t)atoi(argv[NUM_CLIENTS]);
	state->interval = (uint32_t)atoi(argv[INTERVAL]);
	state->srv_port = (uint32_t)atoi(argv[SRV_PORT]);

	return 0;
}

int32_t main_fork_clients (struct child_client *clients, struct prog_state *state)
{
	child_count = 0;

	int in_pipe[2];
	if (pipe(in_pipe) < 0) {
		fprintf(stderr, "Failed to create receive pipe\n");
		return -1;
	}

	struct child_client *client;
	pid_t pid;
	int out_pipe[2];
	for (uint32_t i = 0; i < state->num_client; i++) {
		// Set client struct and its output pipe
		client = &(clients[i]);
		client->in_pipe = in_pipe[1];
		
		// Create client input pipe
		if (pipe(out_pipe) < 0) {
			fprintf(stderr, "Failed to create transmit pipe for client %u\n", i);
			return -1;
		}
		client->in_pipe = out_pipe[1];

		// Fork the client
		printf("Forking client %u...\n", i);
		pid = fork();

		// Check if client or master
		if (pid < 0) {
			// Fork failure
			fprintf(stderr, "Failed to fork client %u!\n", i);
			return -1;
		} else if (pid == 0) {
			// Is child
			close(out_pipe[1]); // Don't write to master's transmit pipe
			close(in_pipe[0]); // Don't read from master's receive pipe
			state->in_pipe = out_pipe[0];
			state->out_pipe = in_pipe[1];
			state->cli_num = i;
			break;
		} else { // Check if parent
			close(out_pipe[0]); // Don't read from client's receive pipe
			client->pid = pid;
		}
	}

	// Close writing to receive pipe
	if (pid > 0) {
		close(in_pipe[1]); // Don't write to own (master's) receive pipe
		state->in_pipe = in_pipe[0];
		state->out_pipe = 0;
		child_count = state->num_client;
	}
	
	// Child=0, Main=1	
	return pid > 0;
}

int32_t parent_wait (struct child_client *clients, struct prog_state state)
{
	printf("Main thread waiting for children or signal\n");

	struct child_client *child;
	uint32_t serviced_child;
	char msg[MSG_LEN + 1];
	memset(msg, 0, MSG_LEN + 1);
	char *word;
	while (child_count > 0) {
		if (read(state.in_pipe, msg, MSG_LEN) < 0) {
			fprintf(stderr, "Failed to read from serviced child\n");
			return -1;
		}
		uint32_t consumed = 0;
		uint32_t progress = 0;
		while (sscanf(msg + progress, "%d%n", &serviced_child, &consumed) != EOF) {
			progress += consumed;
			if (serviced_child >= state.num_client) {
				fprintf(stderr, "Client number out of bounds\n");
				return -1;
			}
			child = &(clients[serviced_child]);
			
			if (scanf("%ms", &word) == EOF) {
				printf("End of word list\n");
				return 0;
			}
			if (write(child->in_pipe, word, strlen(word) + 1) < 0) {
				fprintf(stderr, "Failed to write to child pipe\n");
				return -1;
			}
			free(word);
		}
	}
	return 1;
}

int32_t client_req_word (char **word, int out_pipe, int in_pipe, uint32_t cli_num)
{
	char msg[MSG_LEN + 1];
	memset(msg, 0, MSG_LEN + 1);
	snprintf(msg, MSG_LEN, "%d ", cli_num);

	if (write(out_pipe, msg, strnlen(msg, MSG_LEN)) < 0) {
		fprintf(stdout, "Failed to make word request for client %u\n", cli_num);
		return -1;
	}

	int32_t read_res = read(in_pipe, msg, MSG_LEN);
	if (read_res < 0) {
		fprintf(stdout, "Failed to read from master for client %u\n", cli_num);
		return -1;
	} else if (read_res == 0) {
		return 1;
	}
	
	if (sscanf(msg, "%ms", word) == EOF) {
		return 1;
	}
	return 0;
}

void run_client (struct prog_state state)
{
#ifdef VERBOSE
	printf(">>>\tEntering client %u\n", state.cli_num);
#endif

	struct timespec stag_delay = {
		.tv_sec = state.interval / 1000,
		.tv_nsec = (state.interval % 1000) * 1000000
	};
	
#ifdef VERBOSE
	printf(">>>\tClient %u: calling socket(protocol_domain, protocol_type, exact_protocol)\n", state.cli_num);
#endif
	int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == 0) {
		fprintf(stderr, ">\tClient %u: could not create a socket\n", state.cli_num);
		exit(EXIT_FAILURE);
	}
#ifdef VERBOSE
	printf(">>>\tClient %u: int socket_fd = socket(AF_INET, SOCK_STREAM, 0)\n", state.cli_num);
#endif

	struct sockaddr_in srv_addr;
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(state.srv_port);

	if (inet_pton(AF_INET, SERVER_ADDR, &srv_addr.sin_addr) <= 0) {
		fprintf(stderr, ">\tClient %u: invalid server address\n", state.cli_num);
		exit(EXIT_FAILURE);
	}

#ifdef VERBOSE
	printf(">>>\tClient %u: calling connect(socket, address, address_size)\n", state.cli_num);
#endif
	if (connect(socket_fd, (struct sockaddr *)&srv_addr, sizeof(srv_addr)) < 0) {
		fprintf(stderr, ">\tClient %u: failed to establish a connection\n", state.cli_num);
		exit(EXIT_FAILURE);
	}
#ifdef VERBOSE
	printf(">>>\tClient %u: connect(socket_fd, &srv_addr, sizeof(srv_addr))\n", state.cli_num);
#endif

	char *buffer = 0;

#ifdef VERBOSE
	printf(">>>\tClient %u: Loop by popping words from pipe and sending them until it is closed\n", state.cli_num);
	printf(">>>\tClient %u: Requesting word buffer from main thread via IPC pipe\n", state.cli_num);
#endif
	uint32_t scan_ret = client_req_word(&buffer, state.out_pipe, state.in_pipe, state.cli_num);
	while (scan_ret == 0) {
		if (scan_ret < 0) {
			fprintf(stderr, ">\tClient %u: cannot retrieve input (%u)\n", state.cli_num, errno);
			exit(EXIT_FAILURE);
		} else if (scan_ret != 0) {
			break;
		}
#ifdef VERBOSE
		printf(">>>\t\tClient %u: calling send(socket, buffer, length, flags)\n", state.cli_num);
#endif
		if (send(socket_fd, buffer, strlen(buffer), 0) < 0) {
			fprintf(stderr, ">\tClient %u: failed to send message (%u)\n", state.cli_num, errno);
		} else {
#ifdef VERBOSE
			printf(">>>\t\tClient %u: send(socket_fd, buffer, strlen(buffer), 0)\n", state.cli_num);
#endif
			time_t now;
			time(&now);
			struct tm *local = localtime(&now);
			printf(">\tClient %u: sent '%s' at %02d:%02d:%02d\n", state.cli_num, buffer, local->tm_hour, local->tm_min, local->tm_sec);
		}
#ifdef VERBOSE
		printf(">>>\t\tClient %u: Freeing buffer\n", state.cli_num);
#endif
		free(buffer);
#ifdef VERBOSE
		printf(">>>\t\tClient %u: Sleeping\n", state.cli_num);
#endif
		nanosleep(&stag_delay, &stag_delay);
#ifdef VERBOSE
		printf(">>>\t\tClient %u: Requesting word buffer from main thread via IPC pipe\n", state.cli_num);
#endif
		scan_ret = client_req_word(&buffer, state.out_pipe, state.in_pipe, state.cli_num);
	}

#ifdef VERBOSE
	printf(">>>\tClient %u: Word pipe was closed\n", state.cli_num);
	printf(">\tClient %u: calling close(socket)\n", state.cli_num);
#endif
	close(socket_fd);
	printf(">\tClient %u: input ended\n", state.cli_num);
	return;
}
