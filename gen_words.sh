#!/bin/bash

DICT="/usr/share/dict/words"
SHUF="/usr/bin/shuf"

if ! [ $1 -ge 0 ] &> /dev/null; then
	echo "Usage: $0 [NUM_WORDS]"
	exit -1
fi

if ! [ -f "$DICT" ]; then
	echo "There is no dictionary at \"$DICT\"" >> /dev/stderr
	exit -1
fi

if ! [ -f "$SHUF" ]; then
	echo "There is no shuffle command \"$SHUF\"" >> /dev/stderr
	exit -1
fi

eval " $SHUF -n $1 $DICT"
exit 0
