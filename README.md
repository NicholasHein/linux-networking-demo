# Linux Networking Demo

## Build

```bash
$ make
```

### Clean build

```bash
$ make clean && make
```

### Build Options

 - Debug: Enable GDB symbols and set the `DEBUG` macro
 - Verbose: Print more details to output and set `VERBOSE` macro

```bash
$ make DEBUG=true VERBOSE=true
```

## Usage

### Server

```bash
$ ./server [PORT] [DELAY_MS]
```

#### Example Usage

```bash
$ ./server 8080 2000
```

### Client

```bash
$ ./client [NUM_CLIENTS] [INTERVAL_MS] [PORT] <[TEXT_FILE]
```

#### Example Usage

```bash
$ ./client 3 5000 8080 <./words.txt
```

**OR**

```bash
$ ./gen_words.sh 100 | ./client 3 5000 8080
```

